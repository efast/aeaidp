package com.agileai.hotweb.domain.system;

import java.util.ArrayList;
import java.util.List;

public class FuncHandler {
	private String handlerId = null;
	private String handlerCode = null;
	private String handlerType = null;
	private String handlerURL = null;
	private String functionId = null;
	private List<Operation> operations = new ArrayList<Operation>();
	
	public String getHandlerId() {
		return handlerId;
	}
	public void setHandlerId(String handlerId) {
		this.handlerId = handlerId;
	}
	public String getHandlerCode() {
		return handlerCode;
	}
	public void setHandlerCode(String handlerCode) {
		this.handlerCode = handlerCode;
	}
	public String getHandlerType() {
		return handlerType;
	}
	public void setHandlerType(String handlerType) {
		this.handlerType = handlerType;
	}
	public String getHandlerURL() {
		return handlerURL;
	}
	public void setHandlerURL(String handlerURL) {
		this.handlerURL = handlerURL;
	}
	public List<Operation> getOperations() {
		return operations;
	}
	public String getFunctionId() {
		return functionId;
	}
	public void setFunctionId(String functionId) {
		this.functionId = functionId;
	}
}
