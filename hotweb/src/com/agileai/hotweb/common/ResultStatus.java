package com.agileai.hotweb.common;

public class ResultStatus {
	public static final String Success = "success";
	public static final String Fail = "fail";
	
	private String state = null;
	private String errorMsg = null;
	
	public String getState(){
		return state;
	}
	public String getErrorMsg(){
		return errorMsg;
	}
	public void setSuccess(){
		this.state = Success;
	}
	public void setError(String errorMsg){
		this.state = Fail;
		this.state = errorMsg;
	}
	public boolean isSuccess(){
		return Success.equals(this.state);
	}
	
	public String toString(){
		StringBuffer buffer = new StringBuffer();
		String message = getErrorMsg() == null?"":getErrorMsg();
		buffer.append("{\"state\":\"").append(state).append("\",\"errorMsg\":\"").append(message).append("\"}");
		return buffer.toString();
	}
}