package com.agileai.hotweb.common;

abstract public class Constants {
	public static final String PublicHandlerIdsKey = "publicHandlerIds";
	public static final String OnlineCounterServiceKey = "onlineCounterBeanId";
	
	public static class ConfigFile{
		public static final String SERVICE = "serviceConfigFiles";
		public static final String HANDLER = "handlerConfigFile";
	}
	
	public static class DateTimeFormat{
		public static String DATE = "yyyy-mm-dd";
		public static String TIME = "yyyy-mm-dd hh:mm";
		public static String FULL_TIME = "yyyy-mm-dd hh:mm:ss";
	}
	
	public static class PKType{
		public static final String INCREASE = "increase";
		public static final String ASSIGN = "assign";
		public static final String CHARGEN = "chargen";
	}
	
	public static class FrameHandlers{
		public static String HotwebManageHandlerId = "HotwebManage";
		public static String LogoHandlerId = "Logo";
		public static String LoginHandlerId = "Login";
		public static String HomepageHandlerId = "Homepage";
		public static String MainWinHandlerId = "MainWin";
		public static String MenuTreeHandlerId = "MenuTree";
		public static String NavigaterHandlerId = "Navigater";
	}
}