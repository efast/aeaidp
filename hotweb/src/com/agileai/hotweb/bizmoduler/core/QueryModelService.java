package com.agileai.hotweb.bizmoduler.core;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;

public interface QueryModelService {
	DataRow findDetail(DataParam param);
	List<DataRow> findRecords(DataParam param);
}
