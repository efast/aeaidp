package com.agileai.hotweb.bizmoduler.core;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;

public interface TreeSelectService {
	public List<DataRow> queryPickTreeRecords(DataParam param);
}
