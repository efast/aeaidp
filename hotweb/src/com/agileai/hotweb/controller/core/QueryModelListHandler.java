package com.agileai.hotweb.controller.core;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.QueryModelService;
import com.agileai.hotweb.renders.DispatchRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class QueryModelListHandler extends BaseHandler{
	@SuppressWarnings("rawtypes")
	protected Class detailHandlerClazz = null;
	public QueryModelListHandler(){
		super();
	}
	public ViewRenderer prepareDisplay(DataParam param){
		mergeParam(param);
		initParameters(param);
		this.setAttributes(param);
		List<DataRow> rsList = getService().findRecords(param);
		this.setRsList(rsList);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
	
	public ViewRenderer doQueryAction(DataParam param){
		return prepareDisplay(param);
	}
	public ViewRenderer doViewDetailAction(DataParam param){
		return new DispatchRenderer(getHandlerURL(detailHandlerClazz));
	}

	protected QueryModelService getService() {
		return (QueryModelService)this.lookupService(this.getServiceId());
	}
}