package com.agileai.hotweb.renders;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.agileai.hotweb.domain.PageBean;

public class LocalRenderer extends ViewRenderer {
	public static final String PATH_FIX = "/jsp/"; 
	private static final String PAGE_KEY = "ec_p"; 
	
	private String pageFileUrl = null;
	public LocalRenderer(String pageFileUrl){
		this.pageFileUrl = pageFileUrl;
	}
	public void executeRender(HttpServlet httpServlet,HttpServletRequest request,HttpServletResponse response) throws Exception{
		PageBean pageBean = new PageBean();
		pageBean.setAttributesContainer(handler.getAttributesContainer());
		if (handler.getRsList() != null){
			pageBean.setRsList(handler.getRsList());
		}
		if (handler.getOperaType() != null){
			pageBean.setOperaType(handler.getOperaType());
		}
		pageBean.setHandlerId(handler.getHandlerId());
		pageBean.setHandlerURL(handler.getHandlerURL());
		pageBean.setSessionAttributes(handler.getSessionAttributes());
		request.setAttribute(PAGE_BEAN_KEY, pageBean);
		if (handler.getAttribute(PAGE_KEY) != null){
			String pageNo = (String)handler.getAttribute(PAGE_KEY);
			request.setAttribute(PAGE_KEY,pageNo);	
		}
		
		String fileUrl = PATH_FIX + pageFileUrl;
		RequestDispatcher requestDispatcher = httpServlet.getServletContext().getRequestDispatcher(fileUrl);
		log.info("forwarding to jsp : " + fileUrl);
		requestDispatcher.forward(request, response);
	}
}
