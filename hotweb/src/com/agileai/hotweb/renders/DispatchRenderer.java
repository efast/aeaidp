package com.agileai.hotweb.renders;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.util.StringUtil;

public class DispatchRenderer extends ViewRenderer {
	private String dispatchUrl = null;
	public DispatchRenderer(String dispatchUrl){
		this.dispatchUrl = dispatchUrl;
	}
	public void executeRender(HttpServlet httpServlet,HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		String url = this.dispatchUrl;
		if (!this.dispatchUrl.startsWith("/")){
			if (this.dispatchUrl.equals("http://")
					|| this.dispatchUrl.equals("https://")){
				url = this.dispatchUrl;
			}else{
				url = "/" + this.dispatchUrl;
			}
		}
		String actionType = request.getParameter(BaseHandler.ACTION_TYPE);
		if (url.indexOf(BaseHandler.ACTION_TYPE) == -1 
				&& !StringUtil.isNullOrEmpty(actionType)){
			url = url + "&"+BaseHandler.ACTION_TYPE+"=";
		}
		RequestDispatcher requestDispatcher = httpServlet.getServletContext().getRequestDispatcher(url);
		requestDispatcher.forward(request, response);
	}
}
