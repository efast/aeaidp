﻿package com.agileai.miscdp.hotweb.generator.query;

import java.io.File;

import org.dom4j.Document;
import org.dom4j.io.SAXReader;

import com.agileai.miscdp.NoOpEntityResolver;
import com.agileai.miscdp.hotweb.domain.query.QueryFuncModel;
import com.agileai.miscdp.hotweb.generator.Generator;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.util.XmlUtil;
/**
 * Handler配置代码生成器
 */
public class QMHandlerCfgGenerator implements Generator{
	private QueryFuncModel queryFuncModel = null;
	private String configFile = null;
	private String encoding = "UTF-8";
	
	public void generate() {
		try {
	        SAXReader saxReader = new SAXReader();
	        saxReader.setEntityResolver(new NoOpEntityResolver());
	        saxReader.setIncludeExternalDTDDeclarations(false);
	        saxReader.setValidation(false);
	        Document document = saxReader.read(new File(configFile));
	        
	        String listHandlerId = queryFuncModel.getListHandlerId();
	        String listHandlerClass = queryFuncModel.getListHandlerClass();
	        String listJspName = queryFuncModel.getListJspName();
	        MiscdpUtil.newHandlerConfigElement(document,listHandlerId,listHandlerClass,listJspName);
	        
	        if (queryFuncModel.isShowDetail()){
	        	String editHandlerId = queryFuncModel.getEditHandlerId();
		        String editHandlerClass = queryFuncModel.getEditHandlerClass();
		        String editJspName = queryFuncModel.getDetailJspName();
		        MiscdpUtil.newHandlerConfigElement(document,editHandlerId,editHandlerClass,editJspName);	        	
	        }
	        XmlUtil.writeDocument(document, encoding, configFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setConfigFile(String configFile) {
		this.configFile = configFile;
	}

	public void setFuncModel(QueryFuncModel suFuncModel) {
		this.queryFuncModel = suFuncModel;
	}
}
