package com.agileai.miscdp.hotweb.ui.editors.project;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import com.agileai.miscdp.hotweb.domain.FuncProject;
import com.agileai.miscdp.hotweb.ui.editors.SimpleModelEditor;
import com.agileai.miscdp.util.MiscdpUtil;
/**
 * 功能工程编辑器
 */
public class FuncProjectEditor extends SimpleModelEditor{
	private FuncProject funcModule;
	private FuncProjectPanel funcProjectPanel = null;
	
	protected void init() {
		funcModule = (FuncProject)this.getEditorInput();
	}
	public void doSave(IProgressMonitor monitor) {
		String projectName = funcModule.getProjectName();
		String serverPort = funcProjectPanel.getServerPortText().getText();
		MiscdpUtil.updateProjectConfig(projectName, serverPort);
		this.isModified = false;
		firePropertyChange(PROP_DIRTY);
	}
	public void createPartControl(Composite composite) {
		this.funcProjectPanel = new FuncProjectPanel(composite,SWT.NONE,this);
	}
	public FuncProject getFuncProject() {
		return funcModule;
	}
}
