﻿package com.agileai.miscdp.hotweb.ui.editors;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.TableViewer;
/**
 * 上移下移操作
 */
public class UpOrDownOperation {
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void moveDown(TableViewer tableViewer,int selectedIndex){
		List list = (List)tableViewer.getInput();
		int count = list.size();
		if (selectedIndex < count-1 && count > -1){
			List tempList = new ArrayList();
			tempList.addAll(list);
			list.clear();
			for (int i=0;i < tempList.size();i++){
				if (i == selectedIndex){
					list.add(tempList.get(selectedIndex+1));
					list.add(tempList.get(selectedIndex));
					i++;
				}
				else{
					list.add(tempList.get(i));
				}
			}
			tableViewer.refresh();
		}
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void moveUp(TableViewer tableViewer,int selectedIndex){
		if (selectedIndex > 0){
			List list = (List)tableViewer.getInput();
			List tempList = new ArrayList();
			tempList.addAll(list);
			list.clear();
			for (int i=0;i < tempList.size();i++){
				if (i == selectedIndex-1){
					list.add(tempList.get(selectedIndex));
					list.add(tempList.get(selectedIndex-1));
					i++;
				}
				else{
					list.add(tempList.get(i));
				}
			}
			tableViewer.refresh();
		}
	}
}