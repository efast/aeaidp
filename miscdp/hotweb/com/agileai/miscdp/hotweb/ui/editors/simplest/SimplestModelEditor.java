package com.agileai.miscdp.hotweb.ui.editors.simplest;

import java.io.StringWriter;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import com.agileai.miscdp.hotweb.domain.simplest.SimplestFuncModel;
import com.agileai.miscdp.hotweb.ui.editors.SimpleModelEditor;
import com.agileai.miscdp.hotweb.ui.views.TreeObject;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.hotweb.model.simplest.SimplestFuncModelDocument;
import com.agileai.hotweb.model.simplest.SimplestFuncModelDocument.SimplestFuncModel.BaseInfo;
import com.agileai.hotweb.model.simplest.SimplestFuncModelDocument.SimplestFuncModel.BaseInfo.Handler;
import com.agileai.util.FileUtil;
/**
 * 最简模型编辑器
 */
public class SimplestModelEditor extends SimpleModelEditor{
	private SimplestModelPanel simplestModelPanel = null;
	
	public void doSave(IProgressMonitor monitor) {
		try {
			SimplestFuncModel simplestFuncModel = buildFuncModel();
	    	SimplestFuncModelDocument funcModelDocument = (SimplestFuncModelDocument)buildFuncModelDocument(simplestFuncModel);
			StringWriter writer = new StringWriter();
			funcModelDocument.save(writer);
			String funcName = simplestFuncModel.getFuncName();
			String funcSubPkg = simplestFuncModel.getFuncSubPkg();
			
			String funcDefine = writer.toString();
			String funcId = simplestFuncModel.getFuncId();
			String projectName = simplestFuncModel.getProjectName();
	    	IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
			String fileName = MiscdpUtil.getFuncFileName(projectName,funcId);
			FileUtil.writeFile(fileName, funcDefine);
			
			this.isModified = false;
			firePropertyChange(PROP_DIRTY);
			
			TreeViewer funcTreeViewer = funcModelTree.getFuncTreeViewer();
			ISelection selection = funcTreeViewer.getSelection();
			Object obj = ((IStructuredSelection) selection).getFirstElement();
			TreeObject treeObject = null;
			if (obj instanceof TreeObject) {
				treeObject = (TreeObject) obj;
				treeObject.setName(funcName);
				treeObject.setFuncSubPkg(funcSubPkg);
			}
			
			updateIndexFile(projectName);
			
			this.setPartName(funcName);
			funcTreeViewer.refresh();
			
			simplestFuncModel.buildFuncModel(funcDefine);
			project.refreshLocal(IResource.DEPTH_INFINITE, monitor);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public SimplestFuncModelDocument buildFuncModelDocument(SimplestFuncModel simplestFuncModel){
		SimplestFuncModelDocument funcModelDocument = SimplestFuncModelDocument.Factory.newInstance();
		SimplestFuncModelDocument.SimplestFuncModel funcModel = funcModelDocument.addNewSimplestFuncModel();
		BaseInfo baseInfo = funcModel.addNewBaseInfo();
		baseInfo.setDefaultJspName(simplestFuncModel.getDefaultJspName());
		baseInfo.setDefaultJspTitle(simplestFuncModel.getDefaultJspTitle());
		baseInfo.setTemplate(simplestFuncModel.getTemplate());
		Handler handler = baseInfo.addNewHandler();
		handler.setHandlerId(simplestFuncModel.getHandlerId());
		handler.setHandlerClass(simplestFuncModel.getHandlerClass());
		return funcModelDocument;
	}
	public void createPartControl(Composite composite) {
		simplestModelPanel = new SimplestModelPanel(composite,this,SWT.NONE);
	}
	public SimplestFuncModel buildFuncModel() {
		SimplestFuncModel simplestFuncModel = (SimplestFuncModel)this.baseFuncModel;
		String funcName = simplestModelPanel.getFuncNameText().getText();
		String funcSubPkg = simplestModelPanel.getFuncSubPkgText().getText();
		String handlerId = simplestModelPanel.getHandlerIdText().getText();
		String handlerClass = simplestModelPanel.getHandlerClassText().getText();
		String defaultJspName = simplestModelPanel.getPageNameText().getText();
		String defaultJspTitle = simplestModelPanel.getPageTitleText().getText(); 
		String template = simplestModelPanel.getTemplateCombo().getText();
		
		simplestFuncModel.setFuncName(funcName);
		simplestFuncModel.setFuncSubPkg(funcSubPkg);
		simplestFuncModel.setHandlerId(handlerId);
		simplestFuncModel.setHandlerClass(handlerClass);
		simplestFuncModel.setDefaultJspName(defaultJspName);
		simplestFuncModel.setDefaultJspTitle(defaultJspTitle);
		simplestFuncModel.setTemplate(template);
		
		return simplestFuncModel;
	}
}
