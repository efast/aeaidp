package com.agileai.miscdp.hotweb.ui.editors.portlet;

import java.io.StringWriter;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import com.agileai.miscdp.hotweb.domain.portlet.PortletFuncModel;
import com.agileai.miscdp.hotweb.ui.editors.SimpleModelEditor;
import com.agileai.miscdp.hotweb.ui.views.TreeObject;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.hotweb.model.portlet.PortletFuncModelDocument;
import com.agileai.hotweb.model.portlet.PortletFuncModelDocument.PortletFuncModel.BaseInfo;
import com.agileai.util.FileUtil;
/**
 * Portlet模型编辑器
 */
public class PortletModelEditor extends SimpleModelEditor{
	private PortletModelPanel portletModelPanel = null;
	
	public void doSave(IProgressMonitor monitor) {
		try {
			PortletFuncModel simplestFuncModel = buildFuncModel();
	    	PortletFuncModelDocument funcModelDocument = (PortletFuncModelDocument)buildFuncModelDocument(simplestFuncModel);
			StringWriter writer = new StringWriter();
			funcModelDocument.save(writer);
			String funcName = simplestFuncModel.getFuncName();
			String funcSubPkg = simplestFuncModel.getFuncSubPkg();
			
			String funcDefine = writer.toString();
			String funcId = simplestFuncModel.getFuncId();
			String projectName = simplestFuncModel.getProjectName();
	    	IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
			String fileName = MiscdpUtil.getFuncFileName(projectName,funcId);
			FileUtil.writeFile(fileName, funcDefine);
			
			this.isModified = false;
			firePropertyChange(PROP_DIRTY);
			portletModelPanel.keywordText.setFocus();
			
			TreeViewer funcTreeViewer = funcModelTree.getFuncTreeViewer();
			ISelection selection = funcTreeViewer.getSelection();
			Object obj = ((IStructuredSelection) selection).getFirstElement();
			TreeObject treeObject = null;
			if (obj instanceof TreeObject) {
				treeObject = (TreeObject) obj;
				treeObject.setName(funcName);
				treeObject.setFuncSubPkg(funcSubPkg);
			}
			
			updateIndexFile(projectName);
			
			this.setPartName(funcName);
			funcTreeViewer.refresh();
			
			simplestFuncModel.buildFuncModel(funcDefine);
			project.refreshLocal(IResource.DEPTH_INFINITE, monitor);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public PortletFuncModelDocument buildFuncModelDocument(PortletFuncModel portletFuncModel){
		PortletFuncModelDocument funcModelDocument = PortletFuncModelDocument.Factory.newInstance();
		PortletFuncModelDocument.PortletFuncModel funcModel = funcModelDocument.addNewPortletFuncModel();
		BaseInfo baseInfo = funcModel.addNewBaseInfo();
		baseInfo.setKeywords(portletFuncModel.getKeywords());
		baseInfo.setPortletName(portletFuncModel.getPortletName());
		baseInfo.setPortletClass(portletFuncModel.getPortletClass());
		baseInfo.setPortletTitle(portletFuncModel.getPortletTitle());
		baseInfo.setViewJspName(portletFuncModel.getViewJspName());
		baseInfo.setEditJspName(portletFuncModel.getEditJspName());
		baseInfo.setAjaxLoadData(portletFuncModel.isAjaxLoadData());
		baseInfo.setGenerateHelp(portletFuncModel.isGenerateHelp());
		baseInfo.setTemplate(portletFuncModel.getTemplate());
		return funcModelDocument;
	}
	public void createPartControl(Composite composite) {
		portletModelPanel = new PortletModelPanel(composite,this,SWT.NONE);
	}
	
	public PortletFuncModel buildFuncModel() {
		PortletFuncModel portletFuncModel = (PortletFuncModel)this.baseFuncModel;
		String funcName = portletModelPanel.getFuncNameText().getText();
		String funcSubPkg = portletModelPanel.getFuncSubPkgText().getText();
		
		String portletName = portletModelPanel.getPortletIdText().getText();
		String portletClass = portletModelPanel.getPortletClassText().getText();
		String viewPageName = portletModelPanel.getViewPageNameText().getText();
		String editPageName = portletModelPanel.getEditPageNameText().getText();
		String keywords = portletModelPanel.getKeywordText().getText();
		boolean ajaxLoadData = portletModelPanel.getAjaxLoadDataYButton().getSelection();
		boolean generateHelp = portletModelPanel.getGenerateHelpYButton().getSelection();
		String template = portletModelPanel.getTemplateCombo().getText();
		
		portletFuncModel.setFuncName(funcName);
		portletFuncModel.setFuncSubPkg(funcSubPkg);
		portletFuncModel.setPortletName(portletName);
		portletFuncModel.setPortletClass(portletClass);
		portletFuncModel.setPortletTitle(funcName);
		portletFuncModel.setViewJspName(viewPageName);
		portletFuncModel.setEditJspName(editPageName);
		portletFuncModel.setKeywords(keywords);
		portletFuncModel.setAjaxLoadData(ajaxLoadData);
		portletFuncModel.setGenerateHelp(generateHelp);
		portletFuncModel.setTemplate(template);
		
		return portletFuncModel;
	}
}
