package com.agileai.miscdp.hotweb.ui.editors.module;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;

/**
 * 功能模块编辑器容器
 */
public class FuncModulePanel extends Composite {
	private Text moduleDescText;
	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());
	private Text moduleNameText;
	private FuncModuleEditor moduleEditorPart; 

	public FuncModulePanel(Composite parent, int style,FuncModuleEditor editorPart) {
		super(parent, style);
		toolkit.adapt(this);
		toolkit.paintBordersFor(this);
		this.moduleEditorPart = editorPart;
		
		this.createContent();
	}

	private void createContent(){
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		setLayout(gridLayout);

		final Label label = new Label(this, SWT.NONE);
		label.setText("模块名称");

		moduleNameText = new Text(this, SWT.BORDER);
		moduleNameText.setText(moduleEditorPart.getFuncModule().getMouleName());
		moduleNameText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				moduleEditorPart.setModified(true);
				moduleEditorPart.fireDirty();
				moduleEditorPart.getFuncModule().setMouleName(moduleNameText.getText());
			}
		});
		final GridData gd_funcNameText = new GridData(SWT.FILL, SWT.CENTER, true, false);
		moduleNameText.setLayoutData(gd_funcNameText);
		toolkit.adapt(moduleNameText, true, true);

		
		final Label label_1 = new Label(this, SWT.NONE);
		label_1.setLayoutData(new GridData());
		label_1.setText("模块描述");

		moduleDescText = new Text(this, SWT.MULTI | SWT.READ_ONLY | SWT.BORDER);
		moduleDescText.setText("这是一个功能模块（功能目录），不能进行编辑修改操作！");
		toolkit.adapt(moduleDescText, true, true);
		final GridData gd_funcTypeText_1 = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gd_funcTypeText_1.heightHint = 88;
		moduleDescText.setLayoutData(gd_funcTypeText_1);
	}
	
	@Override
	public void dispose() {
		super.dispose();
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	public Text getModuleNameText() {
		return moduleNameText;
	}
}
