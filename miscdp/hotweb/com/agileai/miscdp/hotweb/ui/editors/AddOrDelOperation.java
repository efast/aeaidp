﻿package com.agileai.miscdp.hotweb.ui.editors;

import java.util.List;

import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
/**
 * 添加删除操作
 */
public class AddOrDelOperation {
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void addRow(TableViewer tableViewer,Class clazz){
		try {
			List intpuList = (List)tableViewer.getInput();
			Object addedIntance = clazz.newInstance(); 
			intpuList.add(addedIntance);
			tableViewer.refresh();
			tableViewer.setSelection(new StructuredSelection(addedIntance));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	@SuppressWarnings({"rawtypes" })
	public void delRow(TableViewer tableViewer){
		int selectedIndex = tableViewer.getTable().getSelectionIndex();
		if (selectedIndex != -1){
			List intpuList = (List)tableViewer.getInput();
			intpuList.remove(selectedIndex);
			tableViewer.refresh();
			int newSelectIndex = selectedIndex-1;
			if (newSelectIndex != -1){
				tableViewer.getTable().setSelection(newSelectIndex);	
			}
		}
	}
}
