package com.agileai.miscdp.hotweb.ui.editors;

import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;

import com.agileai.miscdp.hotweb.domain.BaseFuncModel;
import com.agileai.miscdp.hotweb.ui.views.FuncModelTreeView;
import com.agileai.miscdp.hotweb.ui.views.TreeObject;
import com.agileai.miscdp.hotweb.ui.views.TreeViewContentProvider;
import com.agileai.miscdp.util.MiscdpUtil;
/**
 * 简单（单页）功能模型编辑器基类
 */
abstract public class SimpleModelEditor extends EditorPart implements IResourceChangeListener{
	protected BaseFuncModel baseFuncModel;
	protected boolean isModified = false;
	protected FuncModelTreeView funcModelTree = null;
	
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
        setSite(site);
        setInput(input);
        setPartName(input.getName());
		init();
		ResourcesPlugin.getWorkspace().addResourceChangeListener(this);
    }
	protected void init() {
		baseFuncModel = (BaseFuncModel)this.getEditorInput();
	}
	public void dispose() {
		ResourcesPlugin.getWorkspace().removeResourceChangeListener(this);
		super.dispose();
	}
	
	public void doSaveAs() {
	}
	public boolean isSaveAsAllowed() {
		return false;
	}
	public void resourceChanged(final IResourceChangeEvent event){
	}
	public void fireDirty(){
		firePropertyChange(PROP_DIRTY);
	}
	public boolean isDirty() {
		return isModified;
	}
	abstract public void doSave(IProgressMonitor monitor);
	abstract public void createPartControl(Composite composite);
	public void setFocus() {
	}
	public void setModified(boolean isModified) {
		this.isModified = isModified;
	}
	public BaseFuncModel getFuncModel() {
		return baseFuncModel;
	}
	public void setFuncModelTree(FuncModelTreeView funcModelTree) {
		this.funcModelTree = funcModelTree;
	}
	
	protected void updateIndexFile(String projectName){
		TreeViewer funcTreeViewer = funcModelTree.getFuncTreeViewer();
		TreeViewContentProvider treeViewContentProvider = (TreeViewContentProvider)funcTreeViewer.getContentProvider();
		TreeObject projectTreeObject = treeViewContentProvider.getProjectTreeObject(projectName);
		MiscdpUtil.updateTreeIndexFile(projectTreeObject);
	}
	
	public FuncModelTreeView getFuncModelTree() {
		return funcModelTree;
	}
}
