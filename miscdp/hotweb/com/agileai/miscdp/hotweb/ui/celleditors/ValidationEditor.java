package com.agileai.miscdp.hotweb.ui.celleditors;

import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

import com.agileai.miscdp.hotweb.domain.PageFormField;
import com.agileai.miscdp.hotweb.domain.Validation;
import com.agileai.miscdp.hotweb.ui.celleditors.dialog.ValidationDialog;
import com.agileai.miscdp.hotweb.ui.editors.BaseModelEditor;
import com.agileai.miscdp.util.MiscdpUtil;
/**
 * 页面校验属性编辑器
 */
public class ValidationEditor extends DialogCellEditor{
	Table table = null;
	TableViewer tableViewer = null;
	BaseModelEditor suModelEditor = null;
    public ValidationEditor(BaseModelEditor suModelEditor,TableViewer tableViewer,Composite parent) {
        this(parent, SWT.NONE);
        this.table = (Table)parent;
        this.tableViewer = tableViewer;
        this.suModelEditor = suModelEditor;
    }
    public ValidationEditor(Composite parent, int style) {
        super(parent, style);
    }
	@SuppressWarnings("rawtypes")
	protected Object openDialogBox(Control cellEditorWindow) {
    	ValidationDialog dialog = new ValidationDialog(this.getControl().getShell());
    	TableItem[] tableItems = table.getSelection();
		TableItem item = (TableItem)tableItems[0];
		PageFormField pageParameter = (PageFormField) item.getData();
		List validationList = pageParameter.getValidations();
		List inputValidationList = Validation.prepareInput();
    	for (int i=0;i < inputValidationList.size();i++){
    		Validation validation = (Validation)inputValidationList.get(i);
        	if (validationList != null && validationList.size() > 0) {
	    		for (int j=0;j < validationList.size();j++){
	    			Validation temp = (Validation)validationList.get(j);
	          		if (validation.getKey().equals(temp.getKey())){
	          			if ("length_limit".equals(validation.getKey())){
	          				validation.setParam0(temp.getParam0());
	          			}
	        			dialog.addValidation(validation);
	        			break;
	        		}
	    		}
        	}
		}
    	dialog.setInputValidationList(inputValidationList);
        dialog.open();
        if (dialog.getReturnCode() != Dialog.OK){
        	return "";
        }
        List validations = dialog.getValidations();
		if (validations != null && validations != pageParameter.getValidations()){
			pageParameter.getValidations().clear();
			for (int i=0;i < validations.size();i++){
				Validation temp = (Validation)validations.get(i);
				pageParameter.getValidations().add(temp);	
			}
		}
		if (!dialog.isCancelEdit()){
			suModelEditor.setModified(true);
			suModelEditor.fireDirty();
			tableViewer.refresh();
		}
        return MiscdpUtil.getValidation(validations);
    }
}
