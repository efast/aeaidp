package com.agileai.miscdp.hotweb.ui.celleditors.dialog;

import java.util.ArrayList;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.agileai.domain.InitParam;
import com.agileai.domain.KeyNamePair;
import com.agileai.miscdp.DeveloperConst;
import com.agileai.miscdp.hotweb.domain.CodeGroup;
import com.agileai.miscdp.hotweb.domain.CodeType;
import com.agileai.miscdp.hotweb.domain.DefValue;
import com.agileai.miscdp.hotweb.domain.PageFormField;
/**
 * 组合编辑器提示框
 */
public class CompositeProviderDialog extends Dialog {
	private ListViewer defValueListViewer;
	private String tagType = null;
	private String dataType = null;
	private String code = null;
	
	private Text codeText;
	private ListViewer codeTypeListViewer;
	private ComboViewer comboViewer;
	private boolean canInitInput = false;
	private Combo combo;
	private List list;
	private PageFormField pageParameter = null;
	@SuppressWarnings("rawtypes")
	private java.util.List devValueInputList;
	
	boolean cancelEdit = true;
	
	private String projectName = null;
	public CompositeProviderDialog(Shell parentShell,String projectName) {
		super(parentShell);
		this.projectName = projectName;
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		container.setLayout(gridLayout);

		final Label label_1 = new Label(container, SWT.NONE);
		label_1.setLayoutData(new GridData());
		label_1.setText("字段编码");

		codeText = new Text(container, SWT.BORDER);
		codeText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				code = codeText.getText();
				pageParameter.setCode(code);
			}
		});
		final GridData gd_codeText = new GridData(SWT.FILL, SWT.CENTER, true, false);
		codeText.setLayoutData(gd_codeText);
		codeText.setText(this.code);
		
		final Label label = new Label(container, SWT.NONE);
		label.setLayoutData(new GridData());
		label.setText("编码分组");

		comboViewer = new ComboViewer(container, SWT.BORDER);
		comboViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			public void selectionChanged(SelectionChangedEvent event) {
				if (canInitInput){
					StructuredSelection selection = (StructuredSelection)comboViewer.getSelection();
					KeyNamePair keyNamePair = (KeyNamePair)selection.getFirstElement();
					codeTypeListViewer.setInput(CodeType.prepareInput(projectName,keyNamePair.getKey()));
					codeTypeListViewer.refresh();
				}
			}
		});
		combo = comboViewer.getCombo();
		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		comboViewer.setLabelProvider(new KeyNamePairLabelProvider());
		comboViewer.setContentProvider(new ArrayContentProvider());
		if (canInitInput){
			comboViewer.setInput(CodeGroup.prepareInput());			
		}

		final Label label_2 = new Label(container, SWT.NONE);
		label_2.setLayoutData(new GridData());
		label_2.setText("编码类型");
		
		codeTypeListViewer = new ListViewer(container, SWT.V_SCROLL | SWT.BORDER);
		codeTypeListViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			public void selectionChanged(SelectionChangedEvent event) {
				if (canInitInput && defValueListViewer != null){
					StructuredSelection selection = (StructuredSelection)codeTypeListViewer.getSelection();
					KeyNamePair keyNamePair = (KeyNamePair)selection.getFirstElement();
					
					String provider = keyNamePair.getKey();
					devValueInputList = DefValue.prepareInput(projectName,tagType,dataType,provider);
					defValueListViewer.setInput(devValueInputList);
					defValueListViewer.refresh();
				}
			}
		});
		list = codeTypeListViewer.getList();
		list.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				StructuredSelection selection = (StructuredSelection)codeTypeListViewer.getSelection();
				KeyNamePair keyNamePair  = (KeyNamePair)selection.getFirstElement();
				String provider = keyNamePair.getKey();
				pageParameter.setValueProvider(provider);
			}
		});
		final GridData gd_list = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_list.heightHint = 155;
		list.setLayoutData(gd_list);
		codeTypeListViewer.setLabelProvider(new KeyNamePairLabelProvider());
		codeTypeListViewer.setContentProvider(new ArrayContentProvider());
	
		if (canInitInput){
			java.util.List inputList = CodeType.prepareInput(projectName,null);
			codeTypeListViewer.setInput(inputList);
			codeTypeListViewer.refresh();
			java.util.List selectedList = getSelectedCodeTypeList(inputList);
			if (selectedList != null && selectedList.size() > 0){
				codeTypeListViewer.setSelection(new StructuredSelection(selectedList));				
			}
		}
		else{
			java.util.List defaultList = new ArrayList();
			CodeType codeType = new CodeType();
			codeType.put("",DeveloperConst.BLANK_STRING);
			defaultList.add(codeType);
			codeTypeListViewer.setInput(defaultList);
		}

		final Label label_2_1 = new Label(container, SWT.NONE);
		label_2_1.setLayoutData(new GridData());
		label_2_1.setText("默认值");

		defValueListViewer = new ListViewer(container, SWT.V_SCROLL | SWT.BORDER);
		List defValueList = defValueListViewer.getList();
		defValueList.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				StructuredSelection selection = (StructuredSelection)defValueListViewer.getSelection();
				KeyNamePair keyNamePair  = (KeyNamePair)selection.getFirstElement();
				String defValue = keyNamePair.getKey();
				if (InitParam.reserveWordsList.contains(defValue)){
					String defValueTemp = getDefaultValue(defValue);
					pageParameter.setDefaultValue(defValueTemp,true);
				}
				else{
					pageParameter.setDefaultValue(defValue,false);	
				}
			}
		});
		defValueListViewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				buttonPressed(IDialogConstants.OK_ID);
			}
		});
		defValueList.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		defValueListViewer.setLabelProvider(new KeyNamePairLabelProvider());
		defValueListViewer.setContentProvider(new ArrayContentProvider());
		String defaultValue = getSrcDefaultValue(pageParameter.getDefaultValue());
		if (defaultValue != null){
			String provider = pageParameter.getValueProvider();
			devValueInputList = DefValue.prepareInput(projectName,tagType,dataType,provider);
			defValueListViewer.setInput(devValueInputList);
			java.util.List defaultList = getSelectedDefValueList(devValueInputList,defaultValue);
			defValueListViewer.setSelection(new StructuredSelection(defaultList));
		}
		return container;
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private java.util.List getSelectedCodeTypeList(java.util.List inputList){
		java.util.List result = new ArrayList();
		String currentKey = pageParameter.getValueProvider();
		if (currentKey != null){
			for (int i=0;i < inputList.size();i++){
				CodeType codeType = (CodeType)inputList.get(i);
				if (codeType.getKey().equals(currentKey)){
					result.add(codeType);
					break;
				}
			}
		}
		return result;
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private java.util.List getSelectedDefValueList(java.util.List inputList,String defValueKey){
		java.util.List result = new ArrayList();
		for (int i=0;i < inputList.size();i++){
			DefValue defValue = (DefValue)inputList.get(i);
			if (defValue.getKey().equals(defValueKey)){
				result.add(defValue);
				break;
			}
		}
		return result;
	}
	public boolean isCancelEdit() {
		return cancelEdit;
	}
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}
	@Override
	protected Point getInitialSize() {
		return new Point(317, 416);
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("内容选择框");
	}
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID) {
			cancelEdit = false;
		}
		if (buttonId == IDialogConstants.CANCEL_ID) {
			this.code = null;
		}
		super.buttonPressed(buttonId);
	}
	public void setCanInitInput(boolean canInitInput) {
		this.canInitInput = canInitInput;
	}
	public String getCode() {
		return code;
	}
	public void setPageParameter(PageFormField pageParameter) {
		this.pageParameter = pageParameter;
		code = pageParameter.getCode();
		dataType =pageParameter.getDataType();
		tagType = pageParameter.getTagType();
	}
	
	private String getDefaultValue(String value){
		String result = "";
		if (InitParam.ReserveWords.TODAY.equals(value)){
			result = "DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL)";
		}
		else if (InitParam.ReserveWords.YESTERDAY.equals(value)){
			result = "DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(new Date(),DateUtil.DAY, -1))";
		}
		else if (InitParam.ReserveWords.TOMORROW.equals(value)){
			result = "DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(new Date(),DateUtil.DAY, 1))";
		}
		else if (InitParam.ReserveWords.DAY_PAST_TWO.equals(value)){
			result = "DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(new Date(),DateUtil.DAY, -2))";
		}
		else if (InitParam.ReserveWords.DAY_NEXT_TWO.equals(value)){
			result = "DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(new Date(),DateUtil.DAY, 2))";
		}
		else if (InitParam.ReserveWords.MONTH_BEGIN.equals(value)){
			result = "DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getBeginOfMonth(new Date()))";
		}
		else if (InitParam.ReserveWords.MONTH_END.equals(value)){
			result = "DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getEndOfMonth(new Date()))";
		}
		else{
			result = "";
		}		
		return result;
	}
	private String getSrcDefaultValue(String value){
		String result = "";
		if ("DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL)".equals(value)){
			result = InitParam.ReserveWords.TODAY;
		}
		else if ("DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(new Date(),DateUtil.DAY, -1))".equals(value)){
			result = InitParam.ReserveWords.YESTERDAY;
		}
		else if ("DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(new Date(),DateUtil.DAY, 1))".equals(value)){
			result = InitParam.ReserveWords.TOMORROW;
		}
		else if ("DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(new Date(),DateUtil.DAY, -2))".equals(value)){
			result = InitParam.ReserveWords.DAY_PAST_TWO;
		}
		else if ("DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(new Date(),DateUtil.DAY, 2))".equals(value)){
			result = InitParam.ReserveWords.DAY_NEXT_TWO;
		}
		else if ("DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getBeginOfMonth(new Date()))".equals(value)){
			result = InitParam.ReserveWords.MONTH_BEGIN;
		}
		else if ("DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getEndOfMonth(new Date()))".equals(value)){
			result = InitParam.ReserveWords.MONTH_END;
		}
		else{
			result = value;
		}		
		return result;
	}
}
