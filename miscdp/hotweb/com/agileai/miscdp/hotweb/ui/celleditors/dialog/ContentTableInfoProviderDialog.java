package com.agileai.miscdp.hotweb.ui.celleditors.dialog;

import net.sf.swtaddons.autocomplete.combo.AutocompleteComboInput;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.agileai.miscdp.hotweb.database.DBManager;
import com.agileai.miscdp.hotweb.domain.treecontent.ContentTableInfo;
import com.agileai.miscdp.util.DialogUtil;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.util.StringUtil;
/**
 * 内容表信息编辑器提示框
 */
public class ContentTableInfoProviderDialog extends Dialog {
	private Combo tableModeCombo;
	private Combo tableNameCombo;
	private String[] tables = null;
	private ContentTableInfo contentTableInfo = null;
	boolean cancelEdit = true;
	
	private String currentTableName = null;
	private Combo foreignKeyCombo;
	private Combo relTableNameCombo;
	
	private AutocompleteComboInput midTableCCI = null;
	private String projectName = null;
	
	public ContentTableInfoProviderDialog(Shell parentShell,String projectName) {
		super(parentShell);
		this.projectName = projectName;
	}
	
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 3;
		container.setLayout(gridLayout);

		final Label tableNameLabel = new Label(container, SWT.NONE);
		tableNameLabel.setText("实体表名");

		tableNameCombo = new Combo(container, SWT.NONE);
		tableNameCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		tableNameCombo.setItems(this.getTables());
		new AutocompleteComboInput(tableNameCombo);
		new Label(container, SWT.NONE);
		
		final Label editModelLabel = new Label(container, SWT.NONE);
		editModelLabel.setText("关联模式");
		
		tableModeCombo = new Combo(container, SWT.READ_ONLY);
		tableModeCombo.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				processRelationConfig();
			}
		});
		tableModeCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		tableModeCombo.setItems(ContentTableInfo.tableModes);
		
		Button refreshButton = new Button(container, SWT.NONE);
		refreshButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				processRelationConfig();
			}
		});
		refreshButton.setText("刷新");

		final Label foreignKeyLabel = new Label(container, SWT.NONE);
		foreignKeyLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		foreignKeyLabel.setText("外键字段");
		
		foreignKeyCombo = new Combo(container, SWT.READ_ONLY);
		foreignKeyCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new Label(container, SWT.NONE);
		
		Label label = new Label(container, SWT.NONE);
		label.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		label.setText("中间表名");
		
		relTableNameCombo = new Combo(container, SWT.NONE);
		relTableNameCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new Label(container, SWT.NONE);
		processRelationConfig();
		this.initValues();
		return container;
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private String[] getTables(){
		if (this.tables == null){
			java.util.List tableList = DBManager.getInstance(projectName).getTables(null);
			this.tables = (String[]) tableList.toArray(new String[0]);	
		}
		return this.tables;
	}
	
	private void initValues(){
		tableNameCombo.setText(contentTableInfo.getTableName());
		tableModeCombo.setText(contentTableInfo.getTableMode());
		foreignKeyCombo.setText(contentTableInfo.getColField());
		relTableNameCombo.setText(contentTableInfo.getRelTableName());
	}
	
	private void processRelationConfig(){
		String selectedTableMode = tableModeCombo.getText();
		if (ContentTableInfo.TableMode.One2Many.equals(selectedTableMode)){
			relTableNameCombo.setItems(new String[]{});
			relTableNameCombo.setText("");
			if (midTableCCI != null){
				midTableCCI = null;				
			}
			String tableName = tableNameCombo.getText();
			if (!StringUtil.isNullOrEmpty(tableName)){
				String[] items = MiscdpUtil.getColNames(DBManager.getInstance(projectName), tableName);
				foreignKeyCombo.setItems(items);
			}
		}
		else if (ContentTableInfo.TableMode.Many2ManyAndRel.equals(selectedTableMode)){
			foreignKeyCombo.setItems(new String[]{});
			foreignKeyCombo.setText("");
			
			String[] targetTables = getTables();
			relTableNameCombo.setItems(targetTables);
			midTableCCI = new AutocompleteComboInput(relTableNameCombo);
		}
	}
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID) {
			if (StringUtil.isNullOrEmpty(tableNameCombo.getText())){
				DialogUtil.showInfoMessage("请先选择实体表名！");
				return;
			}
			if (StringUtil.isNullOrEmpty(tableModeCombo.getText())){
				DialogUtil.showInfoMessage("请先选择关联模式！");
				return;
			}
			String selectedTableMode = tableModeCombo.getText();
			if (ContentTableInfo.TableMode.One2Many.equals(selectedTableMode)){
				if (StringUtil.isNullOrEmpty(foreignKeyCombo.getText())){
					DialogUtil.showInfoMessage("请先选择外键字段！");
					return;
				}
			}
			else if (ContentTableInfo.TableMode.Many2ManyAndRel.equals(selectedTableMode)){
				if (StringUtil.isNullOrEmpty(relTableNameCombo.getText())){
					DialogUtil.showInfoMessage("请先选择中间表名！");
					return;
				}
			}
		}
		if (buttonId == IDialogConstants.OK_ID) {
			cancelEdit = false;
			currentTableName = tableNameCombo.getText();
			contentTableInfo.setTableName(currentTableName);
			contentTableInfo.setTableMode(tableModeCombo.getText());
			String selectedTableMode = tableModeCombo.getText();
			if (ContentTableInfo.TableMode.One2Many.equals(selectedTableMode)){
				contentTableInfo.setColField(foreignKeyCombo.getText());	
			}
			else if (ContentTableInfo.TableMode.Many2ManyAndRel.equals(selectedTableMode)){
				contentTableInfo.setRelTableName(relTableNameCombo.getText());
			}
		}
		super.buttonPressed(buttonId);
	}	
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}
	@Override
	protected Point getInitialSize() {
		return new Point(323, 276);
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("信息配置");
	}
	public boolean isCancelEdit() {
		return cancelEdit;
	}
	public ContentTableInfo getContentTableInfo() {
		return contentTableInfo;
	}
	public void setContentTableInfo(ContentTableInfo contentTableInfo) {
		this.contentTableInfo = contentTableInfo;
	}
	public String getCurrentTableName() {
		return currentTableName;
	}
	public Combo getForeignKeyCombo() {
		return foreignKeyCombo;
	}
	public Combo getRelTableNameCombo() {
		return relTableNameCombo;
	}
}
