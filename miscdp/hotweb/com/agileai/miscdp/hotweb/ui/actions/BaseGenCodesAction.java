﻿package com.agileai.miscdp.hotweb.ui.actions;

import java.io.File;
import java.util.HashMap;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;

import com.agileai.miscdp.hotweb.domain.BaseFuncModel;
import com.agileai.miscdp.hotweb.ui.editors.BaseModelEditorContributor;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.domain.KeyNamePair;
/**
 * 代码生成动作
 */
abstract public class BaseGenCodesAction extends Action implements IRunnableWithProgress{
	protected static String GEN_OPTION_JSPPAGE = "JspPage";
	protected static String GEN_OPTION_HANDLER_CONFIG = "HandlerConfig";
	protected static String GEN_OPTION_HANDLER_CLASS = "HandlerClass";
	protected static String GEN_OPTION_SERVICE_CONFIG = "ServiceConfig";
	protected static String GEN_OPTION_SERVICE_INTERFACE = "ServiceInterface";
	protected static String GEN_OPTION_SERVICE_IMPL = "ServiceImpl";
	protected static String GEN_OPTION_SQLMAP_INDEX = "SqlMapIndex";
	protected static String GEN_OPTION_SQLMAP_DEFINE = "SqlMapDefine";
	
	protected HashMap<String,KeyNamePair> genertorMap = null;
	
	protected BaseModelEditorContributor contributor;
	protected BaseFuncModel funcModel;
	private File xmlFile;
	public BaseGenCodesAction() {
	}
	public void setContributor(BaseModelEditorContributor contributor) {
		this.contributor = contributor;
	}
	protected IEditorPart getModelEditor(){
		IWorkbenchPage workbenchPage = contributor.getPage();
		IEditorPart editorPart = workbenchPage.getActiveEditor();
		return editorPart;
	}
	abstract public void run();
	
	public File getXmlFile() {
		String funcId = funcModel.getFuncId();
		String projectName = funcModel.getProjectName();
		this.xmlFile = new File(MiscdpUtil.getFuncFileName(projectName,funcId));
		return xmlFile;
	}
}
