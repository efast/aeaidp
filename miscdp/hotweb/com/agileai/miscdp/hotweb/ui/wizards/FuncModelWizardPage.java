package com.agileai.miscdp.hotweb.ui.wizards;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;

import com.agileai.miscdp.hotweb.ui.celleditors.dialog.KeyNamePairLabelProvider;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.common.IniReader;
/**
 * 功能模型向导页
 */
public class FuncModelWizardPage extends WizardPage {
	private ListViewer listViewer;
	private List list;
	private Text funcNameText;
	private FuncModelWizard funcModelWizard;
	
	public FuncModelWizardPage(FuncModelWizard funcModelWizard,ISelection selection) {
		super("wizardPage");
		this.funcModelWizard = funcModelWizard;
		IniReader reader = MiscdpUtil.getIniReader(); 
		String title = reader.getValue("FunctionWizard","FuncWizardTitle");
		setTitle(title);
		String desc = reader.getValue("FunctionWizard","FuncWizardDesc");
		setDescription(desc);
	}
	
	@SuppressWarnings("rawtypes")
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 2;
		layout.verticalSpacing = 9;
		Label nameLabel;

		nameLabel = new Label(container, SWT.NULL);
		nameLabel.setText("功能名称");

		funcNameText = new Text(container, SWT.BORDER | SWT.SINGLE);
		funcNameText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		funcNameText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}
		});
		setControl(container);

		final Label functypeLabel = new Label(container, SWT.NONE);
		functypeLabel.setText("功能类型");
		
		listViewer = new ListViewer(container, SWT.V_SCROLL | SWT.BORDER);
		list = listViewer.getList();
		list.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				dialogChanged();
			}
		});
		
		final GridData gd_funcTypeCombo = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_funcTypeCombo.heightHint = 174;
		list.setLayoutData(gd_funcTypeCombo);
		listViewer.setLabelProvider(new KeyNamePairLabelProvider());
		listViewer.setContentProvider(new ArrayContentProvider());
		java.util.List modelList = funcModelWizard.getFuncModleList(); 
		listViewer.setInput(modelList);
		listViewer.setSelection(new StructuredSelection(modelList));

		this.setPageComplete(false);
	}
	
	public void dialogChanged() {
		String funcName = funcNameText.getText();
		if (funcName.length() == 0) {
			updateStatus("功能名称不能为空 !");
			return;
		}
		if (listViewer.getSelection().isEmpty()){
			updateStatus("功能类型必须选择 !");
			return;
		}
		updateStatus(null);
	}

	private void updateStatus(String message) {
		setErrorMessage(message);
		setPageComplete(message == null);
	}
	
	public ListViewer getFuncTypeListViewer() {
		return this.listViewer;
	}
	
	public Text getFuncNameText() {
		return funcNameText;
	}
}