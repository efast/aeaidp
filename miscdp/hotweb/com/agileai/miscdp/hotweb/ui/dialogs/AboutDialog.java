package com.agileai.miscdp.hotweb.ui.dialogs;

import java.io.InputStream;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.agileai.miscdp.util.ImageUtil;
import com.agileai.util.FileUtil;
/**
 * 关于Miscdp提示框
 */
public class AboutDialog extends Dialog {

	private Text text;
	/**
	 * Create the dialog
	 * @param parentShell
	 */
	public AboutDialog(Shell parentShell) {
		super(parentShell);
	}

	/**
	 * Create contents of the dialog
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new FormLayout());

		final Canvas canvas = new Canvas(container, SWT.NONE);
		final FormData fd_canvas = new FormData();
		fd_canvas.bottom = new FormAttachment(100, -5);
		fd_canvas.left = new FormAttachment(0, 5);
		fd_canvas.top = new FormAttachment(0, 5);
		fd_canvas.right = new FormAttachment(0, 205);
		canvas.setLayoutData(fd_canvas);

		final Image image = ImageUtil.get("mainpic");
		final Point origin = new Point(0, 0);
		canvas.addPaintListener(new PaintListener() {
			public void paintControl(PaintEvent e) {
				GC gc = e.gc;
				gc.drawImage(image, origin.x, origin.y);
			}}
		);

		text = new Text(container, SWT.READ_ONLY | SWT.WRAP);
		InputStream inputStream = AboutDialog.class.getResourceAsStream("about.txt");
		String aboutText = FileUtil.readFileByLines(inputStream, "GBK");
		text.setText(aboutText);
		final FormData fd_text = new FormData();
		fd_text.right = new FormAttachment(0, 490);
		fd_text.bottom = new FormAttachment(canvas, 287, SWT.TOP);
		fd_text.top = new FormAttachment(canvas, 0, SWT.TOP);
		fd_text.left = new FormAttachment(canvas, 5, SWT.RIGHT);
		text.setLayoutData(fd_text);
		return container;
	}

	/**
	 * Create contents of the button bar
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID,"关 闭",true);
	}

	/**
	 * Return the initial size of the dialog
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(498, 375);
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("关于 Miscdp");
	}

}
