package com.agileai.miscdp.hotweb.ui.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.agileai.miscdp.hotweb.ui.views.FuncModelTreeView;
import com.agileai.miscdp.util.MiscdpUtil;
/**
 * 创建目录提示框
 */
public class CreateDirDialog extends Dialog {
	private String funcPId = "0";
	private String funcPName = null;
	private Text parentText;
	private Text currentText;
	private FuncModelTreeView funcTree = null;
	
	private String funcId;
	private String funcName = null;
	
	public CreateDirDialog(FuncModelTreeView funcTree,Shell parentShell) {
		super(parentShell);
		this.funcTree = funcTree;
	}
	public String getFuncPId() {
		return funcPId;
	}
	public void setFuncPId(String funcPId) {
		this.funcPId = funcPId;
	}
	public void setFuncPName(String funcPName) {
		this.funcPName = funcPName;
	}
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.marginTop = 20;
		gridLayout.numColumns = 2;
		gridLayout.marginLeft = 10;
		gridLayout.verticalSpacing = 2;
		gridLayout.marginWidth = 2;
		container.setLayout(gridLayout);

		final Label parentNodeLabel = new Label(container, SWT.NONE);
		parentNodeLabel.setLayoutData(new GridData());
		String parentNodeName = MiscdpUtil.getIniReader().getValue(CreateDirDialog.class.getSimpleName(),"ParentNode");
		parentNodeLabel.setText(parentNodeName);

		parentText = new Text(container, SWT.READ_ONLY | SWT.BORDER);
		parentText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		parentText.setText(this.funcPName);
		
		final Label label = new Label(container, SWT.HORIZONTAL);
		label.setLayoutData(new GridData());
		String moduleName = MiscdpUtil.getIniReader().getValue(CreateDirDialog.class.getSimpleName(),"ModuleName");
		label.setText(moduleName);

		currentText = new Text(container, SWT.BORDER);
		currentText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		currentText.setFocus();
		return container;
	}
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,true);
		createButton(parent, IDialogConstants.CANCEL_ID,IDialogConstants.CANCEL_LABEL, false);
	}
	@Override
	protected Point getInitialSize() {
		return new Point(399, 226);
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		String createDirDialogTitle = MiscdpUtil.getIniReader().getValue(CreateDirDialog.class.getSimpleName(), "CreateDirDialogTitle"); 
		newShell.setText(createDirDialogTitle);
	}
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.CANCEL_ID) {
			this.close();
			this.setReturnCode(-1);
			return;
		}
		if (buttonId == IDialogConstants.OK_ID) {
			if ("".equals(this.currentText.getText().trim())){
				String confirmTitle = MiscdpUtil.getIniReader().getValue(CreateDirDialog.class.getSimpleName(),"ConfirmTitle");
				String confirmMsg = MiscdpUtil.getIniReader().getValue(CreateDirDialog.class.getSimpleName(),"ConfirmMsg");
				MessageDialog.openInformation(this.getShell(),confirmTitle,confirmMsg);
				return;
			}
			this.insertFuncDir();
			this.funcTree.getFuncTreeViewer().refresh();
			this.close();
			return;
		}
		super.buttonPressed(buttonId);
	}
	private void insertFuncDir(){
		String nextId = String.valueOf(System.currentTimeMillis());
		String newFuncName = this.currentText.getText();
		this.funcId = String.valueOf(nextId);
		this.funcName = newFuncName;
	}
	public String getFuncId() {
		return funcId;
	}
	public String getFuncName() {
		return funcName;
	}
}
