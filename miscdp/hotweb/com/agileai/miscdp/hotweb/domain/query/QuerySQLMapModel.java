package com.agileai.miscdp.hotweb.domain.query;

public class QuerySQLMapModel {
	private String namespace = null;
	private String queryRecordsSQL = null;
	private String findDetailSQL = null;
	public String getNamespace() {
		return namespace;
	}
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
	public String getQueryRecordsSQL() {
		return queryRecordsSQL;
	}
	public void setQueryRecordsSQL(String queryRecordsSQL) {
		this.queryRecordsSQL = queryRecordsSQL;
	}
	public String getFindDetailSQL() {
		return findDetailSQL;
	}
	public void setFindDetailSQL(String findDetailSQL) {
		this.findDetailSQL = findDetailSQL;
	}
}
