﻿package com.agileai.miscdp.hotweb.domain;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import com.agileai.miscdp.DeveloperConst;
/**
 * 功能模块
 */
public class FuncModule implements IEditorInput{
	protected String mouleName = "";
	protected String moduleId = "";
	protected String projectName = "";
	
	protected String editorId = DeveloperConst.MODULE_EDITOR_ID;
	public boolean exists() {
		return false;
	}
	public ImageDescriptor getImageDescriptor() {
		return null;
	}
	public IPersistableElement getPersistable() {
		return null;
	}
	public static String getTipText(String funcId,String funcName){
		return "模块标识："+funcId+"  "+"模块名称："+funcName;
	}
	public String getToolTipText() {
		return getTipText(moduleId, mouleName);
	}
	@SuppressWarnings({"rawtypes" })
	public Object getAdapter(Class adapter) {
		return null;
	}
	public String getName() {
		return this.mouleName;
	}
	public String getMouleName() {
		return mouleName;
	}
	public void setMouleName(String funcName){
		this.mouleName = funcName;
	}
	public String getModuleId() {
		return moduleId;
	}
	public void setModuleId(String funcId) {
		this.moduleId = funcId;
	}
	public String getEditorId() {
		return editorId;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
}
