﻿package com.agileai.miscdp.hotweb.domain;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.agileai.common.IniReader;
import com.agileai.domain.KeyNamePair;
import com.agileai.miscdp.DeveloperConst;
import com.agileai.miscdp.hotweb.database.DBManager;
import com.agileai.miscdp.util.MiscdpUtil;
/**
 * 默认值
 */
public class DefValue extends KeyNamePair{
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List prepareInput(String projectName,String tagType,String dataType,String provider){
		List list = new ArrayList();
		if ("select".equals(tagType) || "radio".equals(tagType)|| "checkbox".equals(tagType)){
			list = prepareInput(projectName,provider);
		}
		else if ("text".equals(tagType)){
			if ("date".equals(dataType)){
				IniReader reader = MiscdpUtil.getIniReader();
				List defValueList = reader.getList("DefValue");
				for (int i=0;i < defValueList.size();i++){
					KeyNamePair keyNamePair = (KeyNamePair)defValueList.get(i);
					list.add(new DefValue().put(keyNamePair.getKey(),keyNamePair.getValue()));					
				}
				list.add(new DefValue().put("",DeveloperConst.BLANK_STRING));
			}
		}
		if (list.size() == 0){
			list.add(new DefValue().put("",DeveloperConst.BLANK_STRING));
		}
		return list;
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static List prepareInput(String projectName,String provider){
		List list = new ArrayList();
		Connection connection = null;
		Statement st = null;
		ResultSet rs = null;
		DBManager dbManager = DBManager.getInstance(projectName);
		try {
			connection = dbManager.createConnection();
			String sql = "select TYPE_ID,CODE_ID,CODE_NAME from sys_codelist";
			if (provider != null){
				sql = sql+" where TYPE_ID = '"+ provider +"' order by CODE_SORT ";
			}
			else{
				sql = sql+" order by CODE_SORT";
			}
			st = connection.createStatement();
			rs = st.executeQuery(sql);
			while(rs.next()){
				DefValue defValue = new DefValue();
				String key = (String)rs.getString("CODE_ID");
				String value = (String)rs.getString("CODE_NAME");
				defValue.put(key,value);
				list.add(defValue);
			}
			list.add(new DefValue().put("",DeveloperConst.BLANK_STRING));
			connection.commit();
		} catch (Exception e) {
			dbManager.rollbackConnection(connection);
		}
		finally{
			dbManager.release(rs,st, connection);
		}
		return list;
	}
}
