package com.agileai.miscdp.hotweb.domain.treecontent;

import java.util.ArrayList;
import java.util.List;

public class TreeContentTableModel {
	private String namespace = null;
	
	private String queryTreeRecordsSQL = null;
	private String queryPickTreeRecordsSQL = null;
	private String queryTreeRecordSQL = null;
	private String insertTreeRecordSQL = null;
	private String updateTreeRecordSQL = null;
	private String deleteTreeRecordSQL = null;
	
	private String queryChildTreeRecordsSQL = null;
	private String queryMaxSortIdSQL = null;
	private String queryCurLevelRecordsSQL = null;
	private boolean manageTree = false;
	
	private List<TabContentTableModel> contentTableInfoList = new ArrayList<TabContentTableModel>();

	public List<TabContentTableModel> getContentTableInfoList() {
		return contentTableInfoList;
	}

	public String getDeleteTreeRecordSQL() {
		return deleteTreeRecordSQL;
	}

	public void setDeleteTreeRecordSQL(String deleteTreeRecordSQL) {
		this.deleteTreeRecordSQL = deleteTreeRecordSQL;
	}

	public String getInsertTreeRecordSQL() {
		return insertTreeRecordSQL;
	}

	public void setInsertTreeRecordSQL(String insertTreeRecordSQL) {
		this.insertTreeRecordSQL = insertTreeRecordSQL;
	}

	public String getQueryChildTreeRecordsSQL() {
		return queryChildTreeRecordsSQL;
	}

	public void setQueryChildTreeRecordsSQL(String queryChildTreeRecordsSQL) {
		this.queryChildTreeRecordsSQL = queryChildTreeRecordsSQL;
	}

	public String getQueryCurLevelRecordsSQL() {
		return queryCurLevelRecordsSQL;
	}

	public void setQueryCurLevelRecordsSQL(String queryCurLevelRecordsSQL) {
		this.queryCurLevelRecordsSQL = queryCurLevelRecordsSQL;
	}

	public String getQueryMaxSortIdSQL() {
		return queryMaxSortIdSQL;
	}

	public void setQueryMaxSortIdSQL(String queryMaxSortIdSQL) {
		this.queryMaxSortIdSQL = queryMaxSortIdSQL;
	}

	public String getQueryPickTreeRecordsSQL() {
		return queryPickTreeRecordsSQL;
	}

	public void setQueryPickTreeRecordsSQL(String queryPickTreeRecordsSQL) {
		this.queryPickTreeRecordsSQL = queryPickTreeRecordsSQL;
	}

	public String getQueryTreeRecordSQL() {
		return queryTreeRecordSQL;
	}

	public void setQueryTreeRecordSQL(String queryTreeRecordSQL) {
		this.queryTreeRecordSQL = queryTreeRecordSQL;
	}

	public String getQueryTreeRecordsSQL() {
		return queryTreeRecordsSQL;
	}

	public void setQueryTreeRecordsSQL(String queryTreeRecordsSQL) {
		this.queryTreeRecordsSQL = queryTreeRecordsSQL;
	}

	public String getUpdateTreeRecordSQL() {
		return updateTreeRecordSQL;
	}

	public void setUpdateTreeRecordSQL(String updateTreeRecordSQL) {
		this.updateTreeRecordSQL = updateTreeRecordSQL;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public boolean isManageTree() {
		return manageTree;
	}

	public void setManageTree(boolean manageTree) {
		this.manageTree = manageTree;
	}
}
