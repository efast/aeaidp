package com.agileai.miscdp.hotweb.domain;

public class PopupValueProvider {
	public static final String HandlerIdKey = "handlerId";
	
	String handlerId = null;

	public String getHandlerId() {
		return handlerId;
	}

	public void setHandlerId(String handlerId) {
		this.handlerId = handlerId;
	}
}
