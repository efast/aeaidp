<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/query",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#import "/common/PageForm.ftl" as Form>
<#visit doc>
<#macro QueryFuncModel>
<#local baseInfo = .node.BaseInfo>
<#local listTableArea = .node.ListView.ListTableArea>
<#local row =listTableArea["fa:Row"][0]>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>${.node.BaseInfo.@listTitle}</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript">
var detailBox;
function showDetailBox(){
	clearSelection();
	if (!isSelectedRow()){
		writeErrorMsg("请先选中一条记录！");
		return;
	}
	if (!detailBox){
		detailBox = new PopupBox('detailBox','明细信息查看',{size:'big',height:'400px',top:'30px'});
	}
	var url = "<%=pageBean.getHandlerURL()%>&actionType=viewDetail&${Util.parseRsIdName(row.@rsIdColumn)}="+$('#${Util.parseRsIdName(row.@rsIdColumn)}').val();
	detailBox.sendRequest(url);
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ToolBar__">
<#if (baseInfo.@showDetail == "true")>
<table class="toolTable" border="0" cellpadding="0" cellspacing="1">
<tr>
	<td onMouseOver="onMover(this);" onMouseOut="onMout(this);" class="bartdx" align="center" onClick="showDetailBox()"><input id="viewDetail" value="&nbsp;" type="button" class="detailImgBtn" title="查看" />查看</td>
	<td onMouseOver="onMover(this);" onMouseOut="onMout(this);" class="bartdx" align="center" onClick="doQuery();"><input id="refreshImgBtn" value="&nbsp;" type="button" class="refreshImgBtn" title="刷新" />刷新</td>
</tr>
</table>
</#if>
</div>
<#if (Util.isValid(.node.ListView.ParameterArea) && Util.isValid(.node.ListView.ParameterArea["fa:FormObject"]))>
<div id="__ParamBar__">
<table class="queryTable"><tr><td>
<@Form.ParameterArea paramArea=.node.ListView.ParameterArea></@Form.ParameterArea>
</td></tr></table>
</div>
</#if>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" <#if Util.isTrue(listTableArea.@exportCsv)>csvFileName="${.node.BaseInfo.@listTitle}.csv"</#if>
retrieveRowsCallback="process" <#if Util.isTrue(listTableArea.@exportXls)>xlsFileName="${.node.BaseInfo.@listTitle}.xls"</#if>
useAjax="true" <#if Util.isTrue(listTableArea.@sortAble)>sortable="true"</#if>
doPreload="false" toolbarContent="<#if Util.isTrue(listTableArea.@pagination)>navigation|pagejump |pagesize </#if>|export|extend|status"
width="100%" <#if Util.isTrue(listTableArea.@pagination)>rowsDisplayed="15"</#if>
listWidth="100%" 
height="390px"
>
<ec:row styleClass="odd" ondblclick="clearSelection();showDetailBox()" oncontextmenu="selectRow(this,${Util.parseRsIdField(row.@rsIdColumn)});refreshConextmenu()" onclick="selectRow(this,${Util.parseRsIdField(row.@rsIdColumn)})">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${r"${GLOBALROWCOUNT}"}" />
<#list row["fa:Column"] as columnVar>
<@Form.Column column=columnVar></@Form.Column>
</#list>
</ec:row>
</ec:table>
<#list row.@rsIdColumn?split(",") as rsId>
<input type="hidden" name="${rsId}" id="${rsId}" value="" />
</#list>
<input type="hidden" name="actionType" id="actionType" />
<script language="JavaScript">
setRsIdTag(${Util.parseRsIdTag(row.@rsIdColumn)});
var ectableMenu = new EctableMenu('contextMenu','ec_table');
<@Form.Validation paramArea=.node.ListView.ParameterArea></@Form.Validation>
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
</#macro>
