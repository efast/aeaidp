<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/treecontent",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#import "/common/PageForm.ftl" as Form>
<#visit doc>
<#macro TreeContentFuncModel>
<#local baseInfo = .node.BaseInfo>
<#local handler = baseInfo.Handler>
<#local service = baseInfo.Service>
<#local detailArea = .node.TreeEditView.TreeEditArea>
package ${Util.parsePkg(handler.@listHandlerClass)};

import java.util.*;

import ${service.@InterfaceName};
import com.agileai.hotweb.bizmoduler.core.TreeAndContentManage;
import com.agileai.hotweb.controller.core.TreeAndContentManageListHandler;
import com.agileai.domain.*;
import com.agileai.util.*;
import com.agileai.hotweb.domain.*;

public class ${Util.parseClass(handler.@listHandlerClass)} extends TreeAndContentManageListHandler{
	public ${Util.parseClass(handler.@listHandlerClass)}(){
		super();
		this.serviceId = buildServiceId(${Util.parseClass(service.@InterfaceName)}.class);
		this.rootColumnId = "${baseInfo.@rootColumnId}";
		<#if (baseInfo.@isManageTree="true" && baseInfo.@treeEditMode="Tabed")>
        this.defaultTabId = ${Util.parseClass(service.@InterfaceName)}.BASE_TAB_ID;
        <#else>
        this.defaultTabId = "${baseInfo.ContentTableInfo[0].@tabId}";	
        </#if>
    	this.columnIdField = "${baseInfo.@columnIdField}";
    	this.columnNameField = "${baseInfo.@columnNameField}";
    	this.columnParentIdField = "${baseInfo.@columnParentIdField}";
    	this.columnSortField = "${baseInfo.@columnSortField}";	
	}
	protected void processPageAttributes(DataParam param) {
		String tabId = param.get(TreeAndContentManage.TAB_ID,this.defaultTabId);
		<#list .node.ContentListTabArea as listView>
		<#local paramArea = listView.ParameterArea>
		<#local listTableArea = listView.ListTableArea>
		if ("${listView.@tabId}".equals(tabId)){
		<#compress>
		<#if Util.isValid(paramArea)>
			<#list paramArea["fa:FormObject"] as formObject><@SetAttribute formAtom=formObject/></#list>
		</#if>
		<#local row =listTableArea["fa:Row"][0]>
		<#list row["fa:Column"] as columnVar><@InitMappingItem column=columnVar></@InitMappingItem></#list>
		</#compress>
		}
		</#list>
		
		<#if (baseInfo.@isManageTree="true" && baseInfo.@treeEditMode="Tabed")>
		<#compress>
		<#if Util.isValid(detailArea)><#list detailArea["fa:FormObject"] as formObject><@SetFormAttribute formAtom=formObject/>
		</#list></#if>
		</#compress>
		</#if>
	}
	protected void initParameters(DataParam param) {
		String tabId = param.get(TreeAndContentManage.TAB_ID,this.defaultTabId);
		<#list .node.ContentListTabArea as listView>
		<#local paramArea = listView.ParameterArea>
		<#local listTableArea = listView.ListTableArea>
		if ("${listView.@tabId}".equals(tabId)){
		<#compress>
		<#if Util.isValid(paramArea)>
		<#list paramArea["fa:FormObject"] as formAtom>
			<#local atom = formAtom.*[0]>
			<#if Util.isValid(atom["fa:DefValue"])>
				<#if Util.isFalse(atom["fa:DefValue"].@isVariable)>initParamItem(param,"${atom["fa:Name"]}","${atom["fa:DefValue"].@value}");<#else>initParamItem(param,"${atom["fa:Name"]}",${atom["fa:DefValue"].@value});
			</#if></#if>
		</#list>
		</#if>
		</#compress>
		}
		</#list>
	}
	
	protected TreeBuilder provideTreeBuilder(DataParam param){
		${Util.parseClass(service.@InterfaceName)} service = this.getService();
		List<DataRow> menuRecords = service.findTreeRecords(new DataParam());
		TreeBuilder treeBuilder = new TreeBuilder(menuRecords,this.columnIdField,this.columnNameField,this.columnParentIdField);
		return treeBuilder;
	}
	
    protected List<String> getTabList(){
		List<String> result = new ArrayList<String>();
		<#if Util.isTrue(baseInfo.@isManageTree) && baseInfo.@treeEditMode = 'Tabed'>
		result.add(${Util.parseClass(service.@InterfaceName)}.BASE_TAB_ID);
		</#if>
		<#list baseInfo.ContentTableInfo as contentTableInfo>
		result.add("${contentTableInfo.@tabId}");
		</#list>
		return result;
    }	
	
	protected ${Util.parseClass(service.@InterfaceName)} getService() {
		return (${Util.parseClass(service.@InterfaceName)})this.lookupService(this.getServiceId());
	}
}
</#macro>

<#macro SetAttribute formAtom><#local type=formAtom.*[0]?node_name?lower_case><#local atom = formAtom.*[0]><#if (type="select" || type="radio" || type="checkbox")>
	setAttribute("${atom["fa:Name"]}",FormSelectFactory.create("${atom["fa:ValueProvider"]}").addSelectedValue(param.get("${atom["fa:Name"]}")));</#if>
</#macro>
<#macro InitMappingItem column>
	<#if Util.isValidValue(column.@mappingItem)>
	initMappingItem("${column.@property}",FormSelectFactory.create("${column.@mappingItem}").getContent());
	</#if>
</#macro>

<#macro SetFormAttribute formAtom><#local type=formAtom.*[0]?node_name?lower_case><#local atom = formAtom.*[0]><#if (type="select" || type="radio" || type="checkbox")>
	setAttribute("${atom["fa:Name"]}",FormSelectFactory.create("${atom["fa:ValueProvider"]}").addSelectedValue(getOperaAttributeValue("${atom["fa:Name"]}","${atom["fa:DefValue"].@value}")));</#if>
</#macro>