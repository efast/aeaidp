<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/pickfill",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#import "/common/PageForm.ftl" as Form>
<#visit doc>
<#macro PickFillFuncModel>
<#local baseInfo = .node.BaseInfo>
<#local service = baseInfo.Service>
package ${Util.parsePkg(service.@ImplClassName)};

import com.agileai.hotweb.bizmoduler.core.PickFillModelServiceImpl;
import ${service.@InterfaceName};

public class ${Util.parseClass(service.@ImplClassName)} extends PickFillModelServiceImpl 
	implements ${Util.parseClass(service.@InterfaceName)} {
	
	public ${Util.parseClass(service.@ImplClassName)}(){
		super();
	}
}
</#macro>
