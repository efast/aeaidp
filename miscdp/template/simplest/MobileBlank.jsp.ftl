<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/simplest",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#import "/common/PageForm.ftl" as Form>
<#visit doc>
<#macro SimplestFuncModel>
<#local baseInfo = .node.BaseInfo>
<!doctype html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html>
<head>
<meta charset="utf-8">
<title>${.node.BaseInfo.@defaultJspTitle}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<%@include file="/jsp/inc/resource4jqm.inc.jsp"%>
<style type="text/css">
span,div{
	text-shadow:none;
	font-size:14px;
}
table {
	border-collapse: collapse;
	border: none;
	width:100%;
}
td {
	font-size: 12px;
	padding: 5px 3px;
	border: solid #000 1px;
	text-shadow:none;
}
th {
	font-size: 13px;
	padding: 5px;
	text-align: center;
	border: solid #000 1px;
	font-weight: normal;
	background-color: #09F;
	color: white;
	text-shadow:none;
}
ul {
	margin: 0px;
	padding: 2px;
}
ul li {
	list-style:none;
	padding: 2px 0px 2px 2px;
}
ol li{
	font-size: 13px;
}
</style>
</head>
<body>
<div data-role="page" id="page">

</div>
</body>
</html>
</#macro>
