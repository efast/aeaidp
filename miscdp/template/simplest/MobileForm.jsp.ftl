<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/simplest",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#import "/common/PageForm.ftl" as Form>
<#visit doc>
<#macro SimplestFuncModel>
<#local baseInfo = .node.BaseInfo>
<!doctype html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html>
<head>
<meta charset="utf-8">
<title>${.node.BaseInfo.@defaultJspTitle}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<%@include file="/jsp/inc/resource4jqm.inc.jsp"%>
<style type="text/css">
.ui-select .ui-btn{
	padding: 8px 2px;
}
.ui-btn ui-input-btn{
	margin-top: 10px;
}
.ui-select .ui-btn span{
	text-align:left;
	padding-left:3px;
}
label.error{
	color:red;
}
#status{
	color:blue;
	padding:5px 5px ;
	margin:5px 0px ;
	border-style:solid; 
	border-width:1px; 
	border-color:blue;
	display:none;
}
</style>
<script type="text/javascript">
$(function(){
	$("#form1").validate({submitHandler:function(){
			$("#submitButton").attr("disabled","disabled");
			$("#status").html("正在提交，请稍等……");
			$("#status").show();
			postRequest("form1",{actionType:"submit",onComplete:function(responseText){
				if ("success"==responseText){
					$("#status").css("color","green");
					$("#status").html("提交成功！许可将在5个工作日发送至接收邮箱"+$("#LU_RCV_MAIL").val());
					$("#submitButton").removeAttr("disabled");
				}
			}});
		},errorPlacement: function(error, element) {  
			if (element.is("select")){
				error.insertBefore(element.parent().parent());
			}
			else if (element.is("input")){
				error.insertBefore(element.parent());			
			}			
		},wrapper: "span"
	});
});
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post" style="padding:15px 5px">
<div class="error"></div>
<div id="status" class="status"></div>
<label for="LU_PRO_TYPE">产品类型：</label>
<select id="LU_PRO_TYPE" name="LU_PRO_TYPE" class="required"><%=pageBean.selectValue("LU_PRO_TYPE")%></select>
<label for="LU_PRO_VERS">产品版本：</label>
<select id="LU_PRO_VERS" name="LU_PRO_VERS" class="required"><%=pageBean.selectValue("LU_PRO_VERS")%></select>
<label for="LU_UPGRATE_TYPE">升级类型：</label>
<select id="LU_UPGRATE_TYPE" name="LU_UPGRATE_TYPE" class="required"><%=pageBean.selectValue("LU_UPGRATE_TYPE")%></select>
<label for="LU_RCV_MAIL">接收邮箱：</label>
<input id="LU_RCV_MAIL" name="LU_RCV_MAIL" type="text" value="<%=pageBean.inputValue("LU_RCV_MAIL")%>" size="24"  class="text required email" minlength="8" maxlength="30" />
<fieldset class="ui-grid">
    <button id="submitButton" type="submit" data-theme="b">提交</button>
</fieldset>
<input type="hidden" name="actionType" id="actionType" value=""/>
</form>
</body>
</html>
</#macro>
