package com.companyname.projectname.bizmoduler.sample;

import com.agileai.hotweb.bizmoduler.core.BaseInterface;

public interface DemoBizManage
        extends BaseInterface {
	public String sayHi(String theGuyName);
}
